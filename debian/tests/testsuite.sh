#!/bin/bash

ARCH=Linux-`uname -m`-gfortran
mkdir -p exe/$ARCH

for i in /usr/bin/cp2k.*; do ln -s $i exe/$ARCH; done

make -f debian/rules override_dh_auto_test

# do_regtest always exits with 0 even if some tests failed so check the summary
# file summary.txt of the latest regtesting run
SUMMARY=$(ls -t TEST-*/summary.txt | head -1)
exit `grep FAILED $SUMMARY | awk '{print $5}'`
